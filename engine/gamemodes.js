const inquirer = require('inquirer');

// Classe principales des modes de jeu
class Gamemodes {
    constructor(gamemode){
        this.players = [];
        this.gamemode = gamemode;
        this.isComplete = false;
        this.shotSinceLastPlayerChange = 0
    }

    static shuffle(players) {
        //échange les joueurs
        for (let i = players.length - 1; i > 0; i--) {
            const res = Math.floor(Math.random() * (i + 1));
            [players[i], players[res]] = [players[res], players[i]];
        }
        return players;
    }

    endGame(){ 
        this.isComplete = true;
        return this.getPlayerTurn()
    }

    getPlayerTurn(){
        return {
            completion: this.isComplete,
            playing: this.players[0],
            nextPlayer: this.players[1]
        }
    }

    isCompleted(){
        return this.isComplete
    }

    getPlayers(){
        return this.players
    }

    getName(){
        return this.gamemode
    }

    addPlayer(player){
        this.players.push(player)
    }

    shufflePlayers(){
        this.players = Gamemodes.shuffle(this.players)
    }

    nextTurn(message){
        this.shotSinceLastPlayerChange++;
        if(this.shotSinceLastPlayerChange >= this.players[0].getMaxShot()){
            this.addPlayer(this.players.shift());
            this.shotSinceLastPlayerChange = 0
        }
        const rep = this.getPlayerTurn();
        if(message !== null && message !== undefined){
            rep.message = message;
        }
        return rep
    }

    getTurnInfos(){
        let rep = '\n';
        //Affiche le nom du joueur qui joue
        if(this.shotSinceLastPlayerChange === 0) {
            rep += `\nC'est au tour de ${this.players[0].getName()}`
        }
        //Le joueur rejoue
        else if(this.shotSinceLastPlayerChange < this.players[0].getMaxShot()){
            rep += `\nEncore à ${this.players[0].getName()}`
        }
        //Affiche l'objectif du joueur
        if(this.players[0].getTarget() !== undefined && this.players[0].getTarget() !== null){
            rep += `\nObjectif: ${this.players[0].getTarget()}`
        }
        //Affiche le score du joueur
        if(this.players[0].getScore() !== undefined && this.players[0].getScore() !== null){
            rep += `\nScore: ${this.players[0].getScore()}`
        }
        //Affiche le nom du prochain joueur qui joue
        if(this.shotSinceLastPlayerChange >= this.players[0].getMaxShot() -1){
            rep += `\nProchain joueur: ${this.players[1].getName()}`;
        }
        return rep
    }
}

module.exports = Gamemodes;