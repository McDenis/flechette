const Gamemodes = require('../gamemodes.js');

//classe du premier mode de jeu
class World extends Gamemodes {
    constructor(){
        super("WorldTour");
        this.targetVictory = 20
    }

    handleShot(secteur, multiply){
        const player = super.getPlayerTurn().playing;
        const playerTarget = player.getTarget();
        if(secteur === playerTarget){
            if(playerTarget === this.targetVictory) {
                //Fin de partie affiche le message du joueur gagnant
                return super.endGame(/*`Partie terminée ! Victoire pour ${player.getName()}`*/);
            }
            //Affichage de l'objectif suivant lorsque le précédent est atteint
            player.setTarget(playerTarget + 1);
            return super.nextTurn(`Objectif atteint. Suivant: ${playerTarget + 1}`)
        }
        return super.nextTurn()
    }
}

module.exports = World;