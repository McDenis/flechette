const Gamemode = require('../gamemodes.js');

class Game301 extends Gamemode{
    constructor(){
        super("301")
    }

    handleShot(secteur, multiply){
        const player = super.getPlayerTurn().playing;
        const shotScore = secteur * multiply;
        //Vérifie si le score fait est inférieur au score restant OU si le shot est égal au score restant et que le multiplicateur est de 2
        if(shotScore < player.getScore() || (shotScore === player.getScore() && multiply === 2)){
            player.updateScore(- shotScore);
            return player.getScore() === 0 ? super.endGame() : super.nextTurn()
        }
        return super.nextTurn()
    }
}

module.exports = Game301;