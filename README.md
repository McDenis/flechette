# Installation

Installation de node et ces modules

 - require inquirer *npm install inquirer*
 - require express *npm install express --save*
 - require path *npm install --save path*
 - require bodyParser *npm install body-parser*
 - require pug *npm install pug*
 - require methodOverride *npm install method-override*
 - require sqlite *npm install sqlite --save*
  
# Lancement de l'application

`node app.js`

Accéder à http://localhost:8080/

- Afficher les joueurs: `/players`
- Afficher les parties: `/games`
- Ajouter un joueur: `/player/new` *Pas de 'S' cela posé problème*
- Ajouter une partie: `/game/new` *Pas de 'S' cela posé problème*
- Editer un joueur: `/players/:id/edit` *Bouton editer sur `/players`*
- Editer une partie: `/games/:id/edit` *Bouton editer sur `/games`*
- Supprimer un joueur: `/players/:id` *Bouton supprimer sur `/players`*
- Supprimer une partie: `/games/:id` *Bouton supprimer sur `/games`*