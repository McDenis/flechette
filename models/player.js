class Player{
    constructor(name, score, target = 1, maxShot = 1){
        this.name = name;
        this.score = score;
        this.target = target;
        this.maxShot = maxShot
    }

    updateScore(nb){
        this.score += nb;
        return this.score
    }

    getScore(){
        return this.score
    }

    getName(){
        return this.name
    }

    getTarget(){
        return this.target
    }

    getMaxShot(){
        return this.maxShot
    }

    setScore(score){
        this.score = score
    }

    setTarget(target){
        this.target = target
    }

    setMaxShot(newShotLeft){
        this.maxShot = newShotLeft
    }
}

module.exports = Player;