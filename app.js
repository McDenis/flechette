/*
-> Nombre de joueur (2 à N)
-> Joueurs (Name)
-> Mode de jeu

-> Input manuel (Chaque joueur rentre le score de la flechette)

-> Premier joueur choisi aléatoirement

1ere partie:
- Accessible directement depuis un terminal (conseil: console.table pour gérer l'affichage)
- L'input du score de chaque joueur se fait via le terminal (conseil: Inquierer)
- Vous devez gérer votre logique avec des classes, et de l'héritage de classe
- (Bonus: le nom du joueur est annoncé au début de son tour, vous pouvez utiliser l'api Dialogflow)
 */
//Import des modules
const inquirer = require('inquirer');
const prompt = inquirer.createPromptModule();

//Import des classes
const Gamemodes = require('./engine/gamemodes.js');
const World = require('./engine/gamemodes/world.js');
const Game301 = require('./engine/gamemodes/game301.js');
const Cricket = require('./engine/gamemodes/cricket.js');
const Player = require('./models/player.js');

//Fonction pour le nombre de joueur
function nbPlayers(){
    return prompt({
        name: 'nbPlayer',
        message: 'Combien de joueurs? ',
        default: 2
    }).then(async (answers) => {
        nbPlayer = answers.nbPlayer;
        while(isNaN(nbPlayer) || nbPlayer < 2){
            nbPlayer = await prompt({
                type: 'number',
                name: 'nbPlayer',
                message: 'Combien de joueurs? ',
            }).then((answers) => {
                return answers.nbPlayer
            })
        }
        return nbPlayer
    })
}

//Fonction du choix du gamemode
function GameMode(){
    return prompt({
        type: 'rawlist',
        name: 'gameMode',
        message: 'Choisissez le mode de jeu: ',
        choices: ['1. WorldTour', '2. 301', '3. Cricket'],
        default: '1'
    }).then(async (answers) => {
        gameMode = answers.gameMode.slice(0,1);
        while(isNaN(gameMode) || gameMode > 3 || gameMode < 0){
            gameMode = await prompt({
                type: 'rawlist',
                name: 'gameMode',
                message: 'Choisissez le mode de jeu: ',
                choices: ['1. WorldTour', '2. 301', '3. Cricket'],
            }).then((answers) => {
                return answers.gameMode.slice(0,1);
            })
        }
        return gameMode;
    })

}

//Fonction pour le nom de chaque joueur
function PlayersName(playerNumber){
    return question = {
        type: 'input',
        name: playerNumber,
        message: `Nom de joueur ${playerNumber}`,
        default: `Player ${playerNumber}`
    }
}

//Fonction pour créer un joueur avec son nom, nombre de joueur dans la partie et le type de partie (worldtour, 301 ou cricket)
function createPlayers(nbPlayer, gameMode, game){
    let questionList = [];
    for(let nb = 1; nb <= nbPlayer; nb++){
        questionList.push(PlayersName(nb))
    }
    return prompt(questionList)
    .then((answers) => {
        for(let nb = 1; nb <= nbPlayer; nb++){
            const player = new Player(answers[nb]);
            //WorldTour
            if(gameMode === 1){player.setTarget(1); player.setMaxShot(3)}
            //301
            if(gameMode === 2){player.setScore(301)}
            //Cricket
            if(gameMode === 3){player.setMaxShot(3)}
            game.addPlayer(player)
        }
    })
}

//Fonction qui permet d'avoir le score qu'a fait le joueur
function shotScore(){
    return prompt([
        {
            type: 'number',
            name: 'sector',
            message: 'Secteur touché (0 - 20, 25)',
        },
        {
            type: 'multiplicator',
            name: 'multiplicator',
            message: 'Multiplicateur touché (1 - 3)',
        },
    ]).then((answers) => {
        if(answers.sector < 0) answers.sector = 0;
        else if(answers.sector > 25) answers.sector = 0;
        else if(answers.sector > 20 && answers.sector < 25) answers.sector = 0;
        if(answers.multiplicator < 1) answers.multiplicator = 1;
        else if(answers.multiplicator > 3) answers.multiplicator = 1;
        if(answers.sector === 25 && answers.multiplicator === 3) answers.multiplicator = 2;

        return answers
    })
}

//Fonction de lancement du jeu
async function startGame(){
    try {
    // Initialisation
        let gameMode = await GameMode().then((rep) => {return rep});
        console.log(gameMode);
        const game = gameMode == 1 ? new World() : (gameMode == 2 ? new Game301() : new Cricket());
        let nbPlayer = await nbPlayers().then((rep) => {return rep});
        await createPlayers(nbPlayer, gameMode, game);
        game.shufflePlayers();

        // Jouer
        let gameResponse;
        do{
            console.log(game.getTurnInfos());
            
            let shot = await shotScore();
            gameResponse = game.handleShot(shot.sector, shot.multiplicator);
            if(gameResponse.message)
                console.log(`\n${gameResponse.message}`)
        }
        while(gameResponse.completion === false);

        console.log(`\nPartie terminée ! Victoire pour ${gameResponse.playing.getName()}`)
    
    } catch (error) {
        console.log(error)
    }
}

//Requis pour api web
const path = require('path');
const bodyParser = require('body-parser');
const pug = require('pug');
const express = require('express');
const app = express();
const methodOverride = require('method-override')
const db = require('sqlite');

app.use(methodOverride('X-HTTP-Method-Override'));
app.use(methodOverride('DELETE'));
app.use(methodOverride('_method'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));

//API - Base de données
db.open('db_dart.db').then(() => {

    /********************************************************************/
    /*                              HOME                                */
    /********************************************************************/

    //Redirection de '/' vers '/games'
    app.get('/', (req, res, next) => {
        res.json({ data: {  message: 'Error 404 : NOT_API_AVAILABLE' } });
        res.redirect(301, '/games');
    });

    /********************************************************************/
    /*                              PLAYERS                             */
    /********************************************************************/

    //Récupération de la listes des joueurs
    app.get('/players', (req, res, next) => {
        db.all("SELECT * FROM players")
            .then((results) => {
                res.results = JSON.stringify(results);
                res.render('players', { results: results });
            })
            .catch((err) => res.send(err));
    });

    //Ajouter d'un joueur (POSTMAN)
    app.post('/players', (req, res, next) => {
        db.run('INSERT INTO players(name, email) VALUES (?, ?)', req.body.name, req.body.email)
            .then((value) => {
                res.redirect(301, '/players');
            })
            .catch((err) => res.send(err));
    });

    //Formulaire de création d'un joueur
    app.get('/player/new', (req, res, next) => {
        res.sendFile(path.join(__dirname + '/forms/players_form.html'));
    });

    //Recupération d'un joueur via son ID
    app.get('/players/:id', (req, res) =>
    {
        db.all("SELECT * FROM players WHERE id_players = (?)", req.params.id)
            .then((value) => {
                res.send(value);
            })
            .catch((err) => res.send(err));
    });

    //Edition d'un joueur via son ID
    app.get('/players/:id/edit', (req, res) =>
    {
        db.all("SELECT * FROM players WHERE id_players = (?)", req.params.id)
            .then((results) => {
                res.results = JSON.stringify(results);
                res.render('players_edit', { results: results });
            })
            .catch((err) => res.send(err));
    });

    //Mettre a jour un joueur
    app.patch('/players/:id', (req, res) =>
    {
        db.all("UPDATE players SET name = (?), email = (?) WHERE id_players = (?)", req.body.name, req.body.email, req.params.id)
            .then((results) => {
                res.redirect(301, '/players');
            })
            .catch((err) => res.send(err));
    });

    //Supprimer un joueur (POSTMAN)
    app.delete('/players/:id', (req, res, next) => {
        db.all("DELETE FROM players WHERE id_players = (?)", req.params.id)
            .then((results) => {
                res.redirect(301, '/players');
            })
            .catch((err) => res.send(err));
    });

    /********************************************************************/
    /*                                 GAMES                            */
    /********************************************************************/

    //Récupération de la listes des parties
    app.get('/games', (req, res, next) => {
        db.all("SELECT * FROM games INNER JOIN status ON games.status = status.id_status INNER JOIN players ON games.players = players.id_players")
            .then((values) => {
                res.values = JSON.stringify(values);
                res.render('games', { values: values });
            })
            .catch((err) => res.send(err));
    });

    //Ajouter d'une partie (POSTMAN)
    app.post('/games', (req, res, next) => {
        db.run('INSERT INTO games(game_name, mode, status, players, created_at) VALUES (?, ?, ?, ?, current_date)', req.body.game_name, req.body.mode, req.body.status, req.body.players)
            .then((values) => {
                res.redirect(301, '/games');
            })
            .catch((err) => res.send(err));
    });

    //Formulaire de création d'une partie
    app.get('/game/new', (req, res, next) => {
        res.sendFile(path.join(__dirname + '/forms/games_form.html'));
    });

    //Recupération d'une partie via son ID
    app.get('/games/:id', (req, res) =>
    {
        db.all("SELECT * FROM games WHERE id_games = (?)", req.params.id)
            .then((values) => {
                res.send(values);
            })
            .catch((err) => res.send(err));
    });

    //Edition d'une partie via son ID
    app.get('/games/:id/edit', (req, res) =>
    {
        db.all("SELECT * FROM games WHERE id_games = (?)", req.params.id)
            .then((values) => {
                res.values = JSON.stringify(values);
                res.render('games_edit', { values: values });
            })
            .catch((err) => res.send(err));
    });

    //Mettre a jour une partie
    app.patch('/games/:id', (req, res) =>
    {
        db.all("UPDATE games SET game_name = (?), mode = (?), status = (?) WHERE id_games = (?)", req.body.game_name, req.body.mode, req.body.status, req.params.id)
            .then((values) => {
                res.redirect(301, '/games');
            })
            .catch((err) => res.send(err));
    });

    //Supprimer une partie (POSTMAN)
    app.delete('/games/:id', (req, res, next) => {
        db.all("DELETE FROM games WHERE id_games = (?)", req.params.id)
            .then((values) => {
                res.redirect(301, '/games');
            })
            .catch((err) => res.send(err));
    });

    /********************************************************************/
    /*                               LANCEMENT                          */
    /********************************************************************/

    app.listen(8080, function () {
        console.log('Port 8080');
        //Lancement du jeu
        startGame();
    });
});

